<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\product\ProductSubCategory */

$this->title = 'Create Product Sub Category';
$this->params['breadcrumbs'][] = ['label' => 'Product Sub Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-sub-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories
    ]) ?>

</div>
