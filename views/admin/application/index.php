<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'phone',
            'text',
            [
                'attribute' => 'ids_product',
                'format' => 'raw',
                'value' => function ($model) {
                    $order_product = ''; 
                    $product = \app\models\product\Product::findOne($model->ids_product);
                    $order_product .= '<a target="_blank" href="'.Url::to(['/product/product', 'id' => $product->id]).'">' . $product->name . '(' . $product->vendor_code . ')' . '</a>';
                    
                    return $order_product;
                },
            ],
            [
                'attribute' => 'is_read',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_read == 0 ? 'Не просмотрен' : 'Просмотрен';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
            ],

        ],
    ]); ?>
</div>
