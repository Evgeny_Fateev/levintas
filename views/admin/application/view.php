<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
         <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
            'text',
            [
                'attribute' => 'ids_product',
                'format' => 'raw',
                'value' => function ($model) {
                    $order_product = '';
                    foreach (unserialize($model->ids_product) as $value){
                        $product = \app\models\product\Product::findOne($value);
                        $order_product .= '<a target="_blank" href="'.Url::to(['/product/product', 'id' => $product->id]).'">' . $product->name . '</a>';
                    }
                    return $order_product;
                },
            ],
        ],
    ]) ?>

</div>
