<?php

use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'initialPreview'=> [
                $model->image != null ? '<img style="max-width:100%;max-height: 100%;" src="'.$model->image.'" class="file-preview-image">' : '',
            ],
            'initialCaption'=> $model->image,
        ],

    ]) ?>

    <?= $form->field($model, 'product_category_id')
        ->dropDownList(ArrayHelper::map($category, 'id', 'name'),
            ['prompt' => 'Выберите категорию', 'onchange'=>'
				$.post( "'.Yii::$app->urlManager->createUrl('admin/product/lists?id=').'"+$(this).val(), function( data ) {
				  $( "select#product-product_sub_category" ).html( data );
				});
			']) ?>

    <?= $form->field($model, 'product_sub_category')->dropDownList(ArrayHelper::map($sub_category, 'id', 'name'), ['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'product_type_id')->dropDownList(ArrayHelper::map($type_category, 'id', 'name'),['prompt' => 'Тип']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'color')->textInput() ?>

    <?= $form->field($model, 'preparation_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gold_content')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_of_stones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight_of_stones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_of_stones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sale')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'new_price')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($all_images, 'image[]')->fileInput(['multiple' => true])
//        ->label(false) ?>
    <?php if(!empty($view_all_images)):?>
        <?php foreach ($view_all_images as $value){
            echo '<img style="max-width:200px%;max-height: 200px; margin-left:20px;" src="'.$value->image.'" class="file-preview-image">' . '<span class="delete-image" data-id="'.$value->id.'" title="Удалить" style="font-size: 25px;color: red;">X</span>';

        }?>
    <?php endif;?>
    <?= $form->field($all_images, 'image[]')->widget(FileInput::classname(), [
        'options' => [ 'multiple' => true,'accept' => 'image/*'],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,

        ],

    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<<JS
    $('.delete-image').on('click', function() {
        var id = $(this).attr('data-id');
        $.post('/admin/product/delete-image', {id:id}, function(response) {
            if(response == 1){
                location.reload();
            }
            else {
                alert('Произошла ошибка при удалении');
            }
        });
    });
JS;
$this->registerJs($js);

?>