<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\product\Product */

$this->title = 'Create Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
        'sub_category' => $sub_category,
        'type_category' => $type_category,
        'all_images' => $all_images,
        'view_all_images' => $view_all_images
    ]) ?>

</div>
