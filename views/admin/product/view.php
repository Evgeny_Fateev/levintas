<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\product\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_category_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productCategory->name;
                },
            ],
            [
                'attribute' => 'product_sub_category',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productSubCategories->name;
                },
            ],
            [
                'attribute' => 'product_type_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productTypes->name;
                },
            ],

            'name',
            [
                'label' => 'Главное изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ],
            'description',
            'price',
            'preparation_time',
            'gold_content',
            'number_of_stones',
            'product_weight',
            'weight_of_stones',
            'type_of_stones',
            'sale',
        ],
    ]) ?>

</div>
