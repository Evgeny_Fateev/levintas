<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'product_category_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productCategory->name;
                },
            ],
            [
                'attribute' => 'product_sub_category',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productSubCategories->name;
                },
            ],
            [
                'attribute' => 'product_type_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->productTypes->name;
                },
            ],

            'name',

            'vendor_code',
            [
                'label' => 'Главное изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ],
            //'price',
            //'preparation_time',
            //'gold_content',
            //'number_of_stones',
            //'product_weight',
            //'weight_of_stones',
            //'type_of_stones',
            //'sale',
            //'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
