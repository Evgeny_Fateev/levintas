<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <a href="/admin/news" class="btn btn-primary btn-lg">Новости</a>
            </div>
            <div class="col-md-4">
                <a href="/admin/product-category" class="btn btn-primary btn-lg">Категории продукта</a>
            </div>
            <div class="col-md-4">
                <a href="/admin/product-sub-category" class="btn btn-primary btn-lg">Подкатегория продукта</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <a href="/admin/product-type" class="btn btn-primary btn-lg">Типы изделий</a>
            </div>
            <div class="col-md-4">
                <a href="/admin/product" class="btn btn-primary btn-lg">Продукты</a>
            </div>
            <div class="col-md-4">
                <a href="/admin/slider" class="btn btn-primary btn-lg">Слайдер главной страницы</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <a href="/admin/feedback" class="btn btn-primary btn-lg">Обратный отзыв</a>
            </div>
            <div class="col-md-4">
                <a href="/admin/application" class="btn btn-primary btn-lg">Заявки(Новых заявок: <b><?= $count_application ?></b>)</a>
            </div>

        </div>




        </div>
    </div>

    <div class="col-md-12" style="margin-top: 20px">


    </div>
</div>