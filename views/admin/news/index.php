<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\News*/
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <p>
        <?= Html::a('Создать новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'label' => 'Изображение',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'style' => 'width:250px;',
                        'class' => 'img-responsive'
                    ]);
                },
            ],
            'name',
            'short_description',
            [
                'attribute' => 'is_published',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->is_published == 0 ? 'Не опубликован' : 'Опубликован';
                },
            ],
            //'short_description',
            //'full_description:ntext',
            //'is_published',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
