<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
<main class="main">
    <div class="main__nav-line">
        <div class="container">
            <a href="#">Главная</a> /
            <span>Новости</span>
        </div>
    </div>

    <div class="articles-page page-content">
        <div class="container">
            <div class="articles-page__content">
                <div class="grid grid_3">
                    <?php foreach ($news as $item):?>
                        <div class="grid__item">
                            <a class="article" href="<?= Url::to(['page/single-news', 'id' => $item->id, 'slug' => $item->slug]) ?>">
                                <div class="article__img object-fit-container cover">
                                    <img src="<?= $item->image ?>" alt="<?= $item->name ?>">
                                </div>
    
                                <div class="article__content">
                                    <div class="article__date">
                                        <span><?= Yii::$app->formatter->asDate($item->created_at)?></span>
                                    </div>

                                    <div class="article__title">
                                        <h2><?= $item->name ?></h2>
                                    </div>

                                    <div class="article__text">
                                        <p><?= $item->short_description ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>

            <?= '<div class="catalog-page__pagination pagination">' . LinkPager::widget([
                'pagination' => $pages_news,
                'prevPageCssClass' => 'prev',
                'nextPageCssClass' => 'next',
            ]) . '</div>'; ?>
        </div>
    </div>
</main>