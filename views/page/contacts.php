<?php if (Yii::$app->session->has('applicationFormSubmitted')) { ?>

    <?php
    $text_app =  'Ваш обратный отзыв оставлен';
    $this->registerJs(
        "alert('$text_app');",
        yii\web\View::POS_READY
    );
    ?>
    <?php Yii::$app->session->remove('applicationFormSubmitted') ?>
<?php } ?>

<main class="main">
    <div class="main__contacts contacts-page page-content page-content_pb0">
        <div class="container">
            <div class="contacts-page__title">
                <h2>Контакты</h2>
            </div>

            <div class="contacts-page__content">
                <div class="contacts-page__item">
                    <div class="contacts-page__item-title">
                        <h3>Приходите к нам</h3>
                    </div>

                    <div class="contacts-page__item-text">
                        <p>г. Москва, ул. Ленина, д. 36/8</p>
                    </div>
                </div>

                <div class="contacts-page__item">
                    <div class="contacts-page__item-title">
                        <h3>Пишите нам</h3>
                    </div>

                    <div class="contacts-page__item-text">
                        <p>email@email.com</p>
                    </div>
                </div>

                <div class="contacts-page__item">
                    <div class="contacts-page__item-title">
                        <h3>Звоните нам</h3>
                    </div>

                    <div class="contacts-page__item-text">
                        <p>+7 (926) 722-47-41</p>
                        <p>+7 (926) 722-47-41</p>
                    </div>
                </div>

                <div class="contacts-page__item">
                    <div class="contacts-page__item-title">
                        <h3>Подпишитесь на нас</h3>
                    </div>

                    <ul class="social">
                        <li><a class="social__item social__item_instagram" href="#" title="Instagram" target="_blank">ARKELL Instagram</a></li>
                        <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte" target="_blank">ARKELL Vkontakte</a></li>
                        <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL Facebook</a></li>
                    </ul>
                </div>
            </div>

            <div class="contacts-page__title">
                <h2>Форма обратной связи</h2>
            </div>

            <?= \app\widgets\FormFeedbackWidget::widget(); ?>
<!--            <form action="#" class="contacts-page__form form">-->
<!--                <div class="contacts-page__form-left">-->
<!--                    <textarea name="" id=""></textarea>-->
<!--                </div>-->
<!---->
<!--                <div class="contacts-page__form-right">-->
<!--                    <input type="text">-->
<!--                    <input type="email">-->
<!--                    <label class="form__check">-->
<!--                        <input type="checkbox" name="agree">-->
<!--                        <span>Согласие на обработку персональных данных</span>-->
<!--                    </label>-->
<!--                    <input type="submit" placeholder="Отправить">-->
<!--                </div>-->
<!---->
<!--            </form>-->
        </div>

        <div id="map"></div>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqj3hW5jwXjY_-0t0nOUFJKaqEVr2x-m4&callback=initMap">
        </script>
        <script>
            var map;
            function initMap() {
                var coordinates = {lat: 47.212325, lng: 38.933663},
                    map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 16,
                        center: coordinates,
                        mapTypeId: 'roadmap',
                        styles: [
                            {
                                "featureType": "administrative",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": "46"
                                    },
                                    {
                                        "color": "#666666"
                                    },
                                    {
                                        "weight": "0.91"
                                    },
                                    {
                                        "gamma": "0.00"
                                    },
                                    {
                                        "lightness": "40"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "saturation": "-5"
                                    },
                                    {
                                        "gamma": "4.31"
                                    },
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "lightness": "-10"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "saturation": "-5"
                                    },
                                    {
                                        "color": "#ff0000"
                                    },
                                    {
                                        "gamma": "8.63"
                                    },
                                    {
                                        "weight": "4.01"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "color": "#666666"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "saturation": "10"
                                    },
                                    {
                                        "color": "#666666"
                                    },
                                    {
                                        "weight": "0.59"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "saturation": "-4"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "saturation": "1"
                                    },
                                    {
                                        "color": "#656565"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "color": "#985959"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.province",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 0
                                    },
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 51
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 30
                                    },
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 40
                                    },
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    },
                                    {
                                        "saturation": "-14"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "hue": "#ffff00"
                                    },
                                    {
                                        "lightness": -25
                                    },
                                    {
                                        "saturation": -97
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "lightness": -25
                                    },
                                    {
                                        "saturation": -100
                                    }
                                ]
                            }
                        ]
                    }),

                    marker = new google.maps.Marker({
                        position: coordinates,
                        map: map,
                        icon: "img/map-marker.png"
                    });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqj3hW5jwXjY_-0t0nOUFJKaqEVr2x-m4&callback=initMap">
        </script>

        <!--
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8000.086608950653!2d30.437943!3d59.915188!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sru!4v1528997908610" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
        -->
    </div>
</main>