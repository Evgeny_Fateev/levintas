<main class="main">
    <div class="main__nav-line">
        <div class="container">
            <a href="#">Главная</a> / <a href="/news">Новости</a> /
            <span><?= $news->name ?></span>
        </div>
    </div>

    <div class="main__article article-page">
        <div class="container">
            <div class="article-page__date">
                <span><?= Yii::$app->formatter->asDate($news->created_at)?></span>
            </div>
            <h1><?= $news->name ?></h1>

            <p><?=  $news->full_description?></p>
        </div>
    </div>
</main>