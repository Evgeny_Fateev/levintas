<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<?php if (Yii::$app->session->has('applicationFormSubmitted')) { ?>

    <?php
    $text_app =  'Ваш заказ оставлен';
    $this->registerJs(
        "alert('$text_app');",
        yii\web\View::POS_READY
    );
    ?>
    <?php Yii::$app->session->remove('applicationFormSubmitted') ?>
<?php } ?>
<main class="main">
    <div class="main__nav-line">
        <div class="container">
            <a href="<?= Url::home()?>">Главная</a> /
            <span>Корзина</span>
        </div>
    </div>

    <div class="card-page page-content">
        <div class="container">
            <div class="card-page__title">
                <h3>Оформить заявку</h3>
            </div>

            <?php $form = ActiveForm::begin([
                'id' =>'form_application',
                'options' => [
                    'class' => 'contacts-page__form form'
                ],
                'validateOnBlur' => false
            ]);?>
            <?= $form->field($model, 'ids_product')
                ->hiddenInput(['value' => !empty(Yii::$app->request->cookies->getValue('basket')) ?  serialize(Yii::$app->request->cookies->getValue('basket')) : '']) ?>

            <div class="contacts-page__form-left">
                <?= $form->field($model, 'text')->textarea(['placeholder' => 'Введите текст']) ?>
            </div>
            <div class="contacts-page__form-right">
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Введите имя']) ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                    'mask' => '+7 (999) 999-9999',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => ('Введите номер телефона')
                    ],
                    'clientOptions' => [
                        'clearIncomplete' => true
                    ]

                ])?>
                <?= $form->field($model, 'is_confirm', ['options' => ['class' => 'form__check']])
                    ->checkbox(['label' => '<span>Я соглашаюсь на обработку персональных данных</span>']); ?>

                <!--    <input type="text">-->
                <!--    <input type="email">-->
                <!--    <label class="form__check">-->
                <!--        <input type="checkbox" name="agree">-->
                <!--        <span>Согласие на обработку персональных данных</span>-->
                <!--    </label>-->
                <input class="g-recaptcha"
                       data-sitekey="6Lev9WAUAAAAAPqfp7Pdc8R54DN8V9Uj8jC49X36"
                       data-callback="onSubmitApplication" type="submit" value="Отправить">
            </div>
            <?php ActiveForm::end();?>
        </div>
    </div>
</main>