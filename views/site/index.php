<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'LEVINTAS JEWERLY | Ювелирные украшения';
?>
<?php if (Yii::$app->session->has('applicationFormSubmitted')) { ?>

    <?php
    $text_app =  'Ваш заказ оставлен';
    $this->registerJs(
        "alert('$text_app');",
        yii\web\View::POS_READY
    );
    ?>
    <?php Yii::$app->session->remove('applicationFormSubmitted') ?>
<?php } ?>

<main class="main page-content page-content_pb0">
    <div class="section about" id="about">
        <div class="container">
            <div class="section__title">
                <h2>О нас</h2>
            </div>

            <div class="about__videos-wrap">
                <div class="about__videos-item js-youtube-wrap">
                    <div class="about__border"></div>
                    <div class="about__youtube js-youtube" id="Kf16ORLNZSI"></div>
                </div>
            </div>

            <a class="section__link" href="/about">Узнать больше</a>
        </div>
    </div>


    <div class="section collections" id="collections">
        <div class="container">
            <div class="section__title">
                <h2>Наши изделия</h2>
            </div>

            <?php foreach ($categories as $category):?>
                <a class="collections__item" href="<?= Url::to(['/product', 'id' => $category['id']]) ?>">
                    <div class="collections__img-wrap">
                        <div class="collections__img collections__img_gold" style="background-image: url(<?= $category['image'] ?>)"></div>
                    </div>

                    <div class="collections__content js-collections-content">
                        <div class="collections__title">
                            <h3><?= $category['name'] ?></h3>
                        </div>

                        <hr>

                        <div class="collections__text">
                            <p><?= $category['description'] ?></p>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>



<!--            <a class="section__link" href="#" title="Смотреть все">Смотреть все</a>-->
        </div>
    </div>


    <div class="services" id="services">
        <div class="services__title-wrap">
            <div class="services__title">
                <h2>Наши услуги</h2>
            </div>

            <hr>

            <div class="services__text">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, inventore!</p>
            </div>
        </div>

        <a class="services__item services__item_order" href="/services">
            <div class="services__item-title">
                <h3>Изготовление<br>на заказ</h3>
            </div>
        </a>

        <a class="services__item services__item_workroom" href="/services">
            <div class="services__item-border"></div>
            <div class="services__item-title">
                <h3>Мастерская<br>по ремонту</h3>
            </div>
        </a>
    </div>

</main>
