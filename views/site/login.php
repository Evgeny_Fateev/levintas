<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container col-sm-4 col-md-offset-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'id' => 'form',
                'validateOnBlur' => false
            ]);?>

            <?= $form->field($model, 'email')->input('email') ?>


            <?= $form->field($model, 'password')->passwordInput()?>


            <?= Html::submitButton('Отправить',
                ['class' => 'btn btn-primary btn-block']) ?><br>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
