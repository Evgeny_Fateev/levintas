<?php
use yii\helpers\Url;
?>
<?php foreach ($products as $product):?>
    <div class="grid__item">
        <a class="product <?= !is_null($product['sale']) ? 'sale' : '' ?>" href="<?= Url::to(['/product/product', 'id' => $product->id]) ?>">
            <?= !is_null($product['sale']) ? '<div class="product__sale">'. $product['sale'] .'</div>' : '' ?>
            <div class="product__img">
                <img src="<?= $product->image ?>" alt="<?= $product->name ?>">
            </div>

<!--            <div class="product__title">-->
<!--                <h3>--><?//= $product->name ?><!--</h3>-->
<!--            </div>-->
            <div class="product__price">
                <span><?= $product->price ?></span>
                <?= !is_null($product['sale']) ? '<span>'. $product['new_price'] .'</span>' : '' ?>
            </div>
        </a>
    </div>
<?php endforeach;?>