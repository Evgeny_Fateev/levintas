<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
<main class="main">
    <div class="main__nav-line">
        <div class="container">
            <a href="<?= Url::home() ?>">Главная</a> /
            <span><?= $category->name ?></span>
        </div>
    </div>

    <div class="main__catalog catalog-page page-content">
        <div class="container">
            <div class="catalog-page__quantity">
                <span><?= $count_product_category ?> <?= $declension_view ?></span>
            </div>

            <form action="" class="catalog-page__filter filter">
                <div class="filter__category">
                    <?php foreach ($product_sub_category as $key => $value): ?>
                        <label>
                            <input value="<?= $value->id ?>" data-id="<?= $category->id ?>" type="radio"
                                   name="category" <?= $key == 0 ? 'checked' : '' ?> >
                            <div class="filter__radio" style="background-image: url(<?= $value->image ?>)">
                                <div class="filter__border"></div>
                                <span><?= $value->name ?></span>
                            </div>
                        </label>
                    <?php endforeach; ?>

                </div>

            </form>

            <div class="result-upload-product"></div>

            <div class="catalog-page__content">
                <div class="grid grid_4">
                    <div class="upload_loader" style="text-align: center; display:none ">
                        <img class="" src="/img/loader.gif" alt="Loading...">
                    </div>
                    <?php foreach ($products as $product): ?>
                        <div class="grid__item">
                            <a class="product <?= !is_null($product['sale']) ? 'sale' : '' ?>"
                               href="<?= Url::to(['/product/product', 'id' => $product->id]) ?>">
                                <?= !is_null($product['sale']) ? '<div class="product__sale">' . $product['sale'] . '</div>' : '' ?>
                                <div class="product__img">
                                    <img src="<?= $product->image ?>" alt="<?= $product->name ?>">
                                </div>

<!--                                <div class="product__title">-->
<!--                                    <h3>--><?//= $product->name ?><!--</h3>-->
<!--                                </div>-->
                                <div class="product__price">
                                    <span><?= $product->price ?></span>
                                    <?= !is_null($product['sale']) ? '<span>'. $product['new_price'] .'</span>' : '' ?>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                    <div class="result-upload-product_load"></div>
                    <div class="catalog-page__load_button" style=" text-align: center;
                        font-size: 24px;
                        background-color: #232326;
                        color: white;
                            margin-top: 21px;
                        border-radius: 6px; display: <?= count($products) != 8 ? 'none' : '' ?>">
                        <button data-id="<?= $product_sub_category[0]['id'] ?>"
                                data-num="8"
                                data-cat="<?= $category->id ?>"
                                class="load_product"
                                type="button">Показать еще</button>
                    </div>
                    <div class="catalog-page__empty" style="display: none">
                        <p>По вашим критериям ничего не найдено</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>