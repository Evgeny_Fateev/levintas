<?php
use yii\helpers\Url;
?>
<main class="main">
    <div class="main__nav-line">
        <div class="container">
            <a href="<?=Url::home()?>">Главная</a> /
            <span><?= $product->productSubCategories->name ?></span>
        </div>
    </div>

    <div class="card-page page-content">
        <div class="container">
            <div class="card-page__title">
                <h2>Описание продукта</h2>
            </div>

            <div class="card-page__columns">
                <p><?= $product->description ?></p>
        </div>
    </div>
</main>