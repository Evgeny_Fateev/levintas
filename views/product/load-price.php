<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
    <div class="catalog-page__content">
        <div class="grid grid_4">
            <div class="upload_loader" style="text-align: center; display:none ">
                <img class="" src="/img/loader.gif" alt="Loading...">
            </div>
            <?php foreach ($products as $product):?>
                <div class="grid__item">
                    <a class="product <?= !is_null($product['sale']) ? 'sale' : '' ?>" href="<?= Url::to(['/product/product', 'id' => $product->id]) ?>">
                        <?= !is_null($product['sale']) ? '<div class="product__sale">'. $product['sale'] .'</div>' : '' ?>
                        <div class="product__img">
                            <img src="<?= $product->image ?>" alt="<?= $product->name ?>">
                        </div>

                        <div class="product__title">
                            <h3><?= $product->name ?></h3>
                        </div>

                        <div class="product__price">
                            <span><?= $product->price ?></span>
                            <?= !is_null($product['sale']) ? '<span>'. $product['new_price'] .'</span>' : '' ?>

                        </div>
                    </a>
                </div>
            <?php endforeach;?>
            <div class="catalog-page__empty" style="display: none">
                <p>По вашим критериям ничего не найдено</p>
            </div>
        </div>
    </div>
<?= '<div class="catalog-page__pagination pagination">' . LinkPager::widget([
    'pagination' => $pages_products,
    'prevPageCssClass' => 'prev',
    'nextPageCssClass' => 'next',
]) . '</div>'; ?>