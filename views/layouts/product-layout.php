<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\product\ProductCategory;
use yii\helpers\Html;
use app\assets\MainAsset;
use \yii\helpers\Url;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta id="m480" name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script>
            if (screen.width < 480) {
                var mvp = document.getElementById('m480');
                mvp.setAttribute('content', 'width=480, user-scalable=0');
            }

            window.addEventListener("resize", function () {
                var mvp = document.getElementById('m480');
                mvp.setAttribute('content', 'width=device-width, user-scalable=0');

                if (screen.width < 480) {
                    var mvp = document.getElementById('m480');
                    mvp.setAttribute('content', 'width=480, user-scalable=0');
                }
            }, false);
        </script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121571917-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-121571917-1');
        </script>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter49411828 = new Ya.Metrika2({
                            id:49411828,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";;

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/49411828"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>


    <div class="loader js-loader">
        <img class="loader__img" src="/img/loader.gif" alt="Loading...">
    </div>

    <div class="page js-page">
        <header class="header <?= $this->params['product_image'] ? 'header_card' : 'js-header'?>">
            <div class="header__row header-row js-header-row">
                <div class="container clearfix">
                    <div class="header-row__left-content">
                        <div class="header-row__phone">
                            <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                        </div>
                    </div>

                    <div class="header-row__logo">
                        <a class="logo" href="<?= Url::home() ?>">
                            <img src="/img/logo.png" alt="ARKELL digital">
                        </a>

                    </div>

                    <div class="header-row__right-content">
                        <ul class="header-row__social social">
                            <li><a class="social__item social__item_instagram" href="#" title="Instagram"
                                   target="_blank">ARKELL
                                    Instagram</a></li>
                            <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte"
                                   target="_blank">ARKELL
                                    Vkontakte</a></li>
                            <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL
                                    Facebook</a></li>
                        </ul>

<!--                        <div class="header-row__favorites">-->
<!--                            <span>--><?//= count(array_filter(Yii::$app->request->cookies->getValue('basket'))) ?><!--</span>-->
<!--                            <a href="/basket">Избранное</a>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>

            <div class="header__menu js-header-menu">
                <div class="container">
                    <div class="menu js-menu">
                        <button class="menu__btn js-menu-btn">
                            <span class="menu__icon"></span>
                        </button>

                        <nav class="menu__content js-menu-content">
                            <ul class="menu__ul">
                                <li>
                                    <a class="js-menu-item menu__link" href="<?= Url::home() ?>" title="Главная">Главная</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="<?= Url::home() ?>#collections"
                                       title="Каталог">Каталог</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/services" title="Услуги">Услуги</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/about" title="О нас">О нас</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/contacts" title="Контакты">Контакты</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/news" title="Новости">Новости</a>
                                </li>
                            </ul>

                            <div class="menu__phone">
                                <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                            </div>

                            <ul class="menu__social social">
                                <li><a class="social__item social__item_instagram" href="#" title="Instagram"
                                       target="_blank">ARKELL Instagram</a></li>
                                <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte"
                                       target="_blank">ARKELL Vkontakte</a></li>
                                <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL
                                        Facebook</a></li>
                            </ul>
                        </nav>
                    </div>


                </div>
            </div>

            <?php if ($this->params['category_image']): ?>
                <div class="header__img" style="background-image: url(<?= $this->params['category_image'] ?>)">
                    <div class="header__bg"></div>
                    <div class="container">
                        <div class="header__content js-header-content">
                            <div class="header__title">
                                <h1><?= $this->params['category'] ?></h1>
                            </div>

                            <div class="header__subtitle">
                                <p><?= $this->params['description'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endif; ?>

            <?php if ($this->params['product_image']): ?>
                <div class="header__img" style="background-image: url(<?= $this->params['product_image_fon'] ?>)">
                    <div class="header__bg"></div>
                    <div class="container">
                        <div class="js-header-content">
                            <div class="header__card card <?= $this->params['product_sale'] ? 'sale' : '' ?>">
                                <div class="js-card-content">
                                    <div class="card__img">
                                        <?= $this->params['product_sale'] ? '<div class="card__sale">'. $this->params['product_sale'] .'</div>' : '' ?>
                                        <div class="card__title">
                                            <h1><?= $this->params['product_name'] ?></h1>
                                        </div>

                                        <div class="card__photo-slider slider-for">
                                            <div class="card__photo-item">
                                                <img src="<?= $this->params['product_image'] ?>" alt="">
                                            </div>
                                            <?php foreach ($this->params['product_images'] as $image) :?>
                                            <div class="card__photo-item">
                                                <img src="<?= $image->image ?>" alt="">
                                            </div>
                                            <?php endforeach;?>
                                        </div>

                                        <div class="card__photo-nav slider-nav">
                                            <div class="card__photo-item">
                                                <img src="<?= $this->params['product_image'] ?>" alt="">
                                            </div>
                                            <?php foreach ($this->params['product_images'] as $image) :?>
                                                <div class="card__photo-item">
                                                    <img src="<?= $image->image ?>" alt="Фото изделия">
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>

                                    <div class="card__content">
                                        <div class="card__title">
                                            <h1><?= $this->params['product_name'] ?></h1>
                                        </div>

                                        <div class="card__section card__section_price">
                                            <div class="card__subtitle">
                                                <h2>Цена</h2>
                                            </div>
                                            <span><?= $this->params['product_price'] ?></span>
                                            <?= $this->params['product_sale'] ? '<span>'. $this->params['product_new_price'] .'</span>' : '' ?>
                                        </div>

                                        <div class="card__section">
                                            <div class="card__subtitle">
                                                <h2>Цвет</h2>
                                            </div>
                                            <span>Белое золото</span>
                                        </div>

                                        <div class="card__section">
                                            <div class="card__subtitle">
                                                <h2>Вес изделия</h2>
                                            </div>
                                            <span><?= $this->params['product_weight'] ?></span>
                                        </div>

                                        <div class="card__section">
                                            <div class="card__subtitle">
                                                <h2>Кол-во камней</h2>
                                            </div>
                                            <span><?= $this->params['number_of_stones'] ?></span>
                                        </div>

                                        <div class="card__section">
                                            <div class="card__subtitle">
                                                <h2>Вес камней</h2>
                                            </div>
                                            <span><?= $this->params['weight_of_stones'] ?></span>
                                        </div>

                                        <button id="add_product_in_basket" data-id="<?= $this->params['product_id'] ?>" class="js-card-btn">Оформить заказ</button>


                                    </div>

                                    <div class="card__footer">
                                        <div class="card__footer-item card__footer-item_time">
                                            <p>Время изготовления: <?= $this->params['preparation_time'] ?></p>
                                        </div>

                                        <div class="card__footer-item card__footer-item_metal">
                                            <p>Проба золота: <?= $this->params['gold_content'] ?></p>
                                        </div>

                                        <div class="card__code">
                                            <p>Артикул изделия: <span class="card__code_value"><?= $this->params['vendor_code'] ?></span></p>
                                        </div>
                                    </div>
                                </div>

                                <?= \app\widgets\OrderWidget::widget(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </header>
        <?= $content ?>


        <footer class="footer">
            <div class="container">
                <div class="footer__menu">
                    <nav>
                        <ul>
                            <li>
                                <a href="<?= Url::home() ?>" title="Главная">Главная</a>
                            </li>
                            <!--
                                            <li>
                                                <a href="catalog.html" title="Каталог">Каталог</a>
                                            </li>
                            -->
                            <li>
                                <a href="/services" title="Услуги">Услуги</a>
                            </li>
                            <li>
                                <a href="/about" title="О нас">О нас</a>
                            </li>
                            <li>
                                <a href="/contacts" title="Контакты">Контакты</a>
                            </li>
                            <li>
                                <a href="/news" title="Новости">Новости</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="footer__catalog">
                    <div class="footer__title">
                        <h2>Наши изделия</h2>
                    </div>

                    <ul>
                        <?php foreach (ProductCategory::find()->asArray()->all() as $value):?>
                            <li>
                                <a href="<?= Url::to(['/product', 'id' => $value['id']]) ?>" title="<?= $value['name'] ?>"><?= $value['name'] ?></a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </div>

                <div class="footer__services">
                    <div class="footer__title">
                        <h2>Услуги</h2>
                    </div>

                    <ul>
                        <li>
                            <a href="/services" title="Главная">Мастерская по ремонту</a>
                        </li>

                        <li>
                            <a href="/services" title="Главная">Изготовление на заказ</a>
                        </li>
                    </ul>
                </div>

                <div class="footer__contacts">
                    <div class="footer__phone">
                        <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                    </div>

                    <ul class="social">
                        <li><a class="social__item social__item_instagram" href="#" title="Instagram" target="_blank">ARKELL
                                Instagram</a></li>
                        <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte" target="_blank">ARKELL
                                Vkontakte</a></li>
                        <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL
                                Facebook</a></li>
                    </ul>
                </div>
            </div>

        </footer>
    </div>
        <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>