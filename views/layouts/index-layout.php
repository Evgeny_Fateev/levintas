<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\product\ProductCategory;
use yii\helpers\Html;
use app\assets\MainAsset;
use yii\helpers\Url;

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta id="m480" name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script>
            if (screen.width < 480) {
                var mvp = document.getElementById('m480');
                mvp.setAttribute('content', 'width=480, user-scalable=0');
            }

            window.addEventListener("resize", function () {
                var mvp = document.getElementById('m480');
                mvp.setAttribute('content', 'width=device-width, user-scalable=0');

                if (screen.width < 480) {
                    var mvp = document.getElementById('m480');
                    mvp.setAttribute('content', 'width=480, user-scalable=0');
                }
            }, false);
        </script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121571917-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-121571917-1');
        </script>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter49411828 = new Ya.Metrika2({
                            id:49411828,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";;

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/49411828"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>



    <div class="loader js-loader">
        <img class="loader__img" src="/img/loader.gif" alt="Loading...">
    </div>

    <div class="page js-page">
        <header class="header js-header">
            <div class="header__row header-row js-header-row">
                <div class="container clearfix">
                    <div class="header-row__left-content">
                        <div class="header-row__phone">
                            <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                        </div>
                    </div>

                    <div class="header-row__logo">
                        <a class="logo" href="<?= Url::home() ?>">
                            <img src="/img/logo.png" alt="ARKELL digital">
                        </a>

                    </div>

                    <div class="header-row__right-content">
                        <ul class="header-row__social social">
                            <li><a class="social__item social__item_instagram" href="#" title="Instagram"
                                   target="_blank">ARKELL
                                    Instagram</a></li>
                            <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte"
                                   target="_blank">ARKELL
                                    Vkontakte</a></li>
                            <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL
                                    Facebook</a></li>
                        </ul>

<!--                        <div class="header-row__favorites">-->
<!--                            <span>--><?//= count(array_filter(Yii::$app->request->cookies->getValue('basket'))) ?><!--</span>-->
<!--                            <a href="/basket">Избранное</a>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>

            <div class="header__menu js-header-menu">
                <div class="container">
                    <div class="menu js-menu">
                        <button class="menu__btn js-menu-btn">
                            <span class="menu__icon"></span>
                        </button>

                        <nav class="menu__content js-menu-content">
                            <ul class="menu__ul">
                                <li>
                                    <a class="js-menu-item menu__link" href="<?= Url::home() ?>" title="Главная">Главная</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link js-menu-scroll" href="#collections"
                                       title="Каталог">Каталог</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link js-menu-scroll" href="#services"
                                       title="Услуги">Услуги</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link js-menu-scroll" href="#about" title="О нас">О
                                        нас</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/contacts"
                                       title="Контакты">Контакты</a>
                                </li>
                                <li>
                                    <a class="js-menu-item menu__link" href="/news" title="Новости">Новости</a>
                                </li>
                            </ul>
                            <div class="menu__phone">
                                <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                            </div>
                            <ul class="menu__social social">
                                <li><a class="social__item social__item_instagram" href="#" title="Instagram"
                                       target="_blank">ARKELL Instagram</a></li>
                                <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte"
                                       target="_blank">ARKELL Vkontakte</a></li>
                                <li><a class="social__item social__item_facebook" href="#" title="Facebook"
                                       target="_blank">ARKELL
                                        Facebook</a></li>
                            </ul>
                        </nav>
                    </div>


                </div>
            </div>
            <div class="header__slider header-slider js-header-slider">
                <?php foreach ($this->params['sliders'] as $item):?>
                    <div class="header-slider__item" style="background-image: url(<?= $item['image'] ?>)">
                        <div class="header-slider__bg"></div>

                        <div class="container">
                            <div class="header-slider__content">
                                <div class="header-slider__title">
                                    <h2><?= $item['title'] ?></h2>
                                </div>

                                <div class="header-slider__text">
                                    <p>Levintas Jewerly</p>
                                </div>

                             </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </header>
        <?= $content ?>


        <footer class="footer">
            <div class="container">
                <div class="footer__menu">
                    <nav>
                        <ul>
                            <li>
                                <a href="<?=Url::home()?>" title="Главная">Главная</a>
                            </li>
                            <!--
                                            <li>
                                                <a href="catalog.html" title="Каталог">Каталог</a>
                                            </li>
                            -->
                            <li>
                                <a href="/services" title="Услуги">Услуги</a>
                            </li>
                            <li>
                                <a href="/about" title="О нас">О нас</a>
                            </li>
                            <li>
                                <a href="/contacts" title="Контакты">Контакты</a>
                            </li>
                            <li>
                                <a href="/news" title="Новости">Новости</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="footer__catalog">
                    <div class="footer__title">
                        <h2>Наши изделия</h2>
                    </div>

                    <ul>
                        <?php foreach (ProductCategory::find()->asArray()->all() as $value):?>
                        <li>
                            <a href="<?= Url::to(['/product', 'id' => $value['id']]) ?>" title="<?= $value['name'] ?>"><?= $value['name'] ?></a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </div>

                <div class="footer__services">
                    <div class="footer__title">
                        <h2>Услуги</h2>
                    </div>

                    <ul>
                        <li>
                            <a href="/services" title="Главная">Мастерская по ремонту</a>
                        </li>

                        <li>
                            <a href="/services" title="Главная">Изготовление на заказ</a>
                        </li>
                    </ul>
                </div>

                <div class="footer__contacts">
                    <div class="footer__phone">
                        <a href="tel:+79267224741" title="Позвонить">+7 (926) 722-47-41</a>
                    </div>

                    <ul class="social">
                        <li><a class="social__item social__item_instagram" href="#" title="Instagram" target="_blank">ARKELL
                                Instagram</a></li>
                        <li><a class="social__item social__item_vkontakte" href="#" title="Vkontakte" target="_blank">ARKELL
                                Vkontakte</a></li>
                        <li><a class="social__item social__item_facebook" href="#" title="Facebook" target="_blank">ARKELL
                                Facebook</a></li>
                    </ul>
                </div>
            </div>

        </footer>
    </div>
        <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>