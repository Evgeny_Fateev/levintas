<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $short_description
 * @property string $full_description
 * @property int $created_at
 * @property int $is_published
 */
class News extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    const PUBLISH = 1;
    const NO_PUBLISH = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image', 'short_description', 'full_description', 'created_at'], 'required', 'on' => self::SCENARIO_CREATE],
            [['created_at', 'is_published'], 'integer', 'on' => self::SCENARIO_UPDATE],
            [['name', 'image'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 500],
            [['full_description'], 'string', 'max' => 30000],
            ['is_published', 'in', 'range' => [self::PUBLISH, self::NO_PUBLISH]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Изображение',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'created_at' => 'Дата создания',
            'is_published' => 'Статус',
        ];
    }

    /**
     * Bevaiors create slug
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'immutable' => false,
                'ensureUnique'=>true,
                'value' => function($event){
                    return \yii\helpers\Inflector::slug($this->translit($event->sender->name));
                },
            ],

        ];
    }
    /**
     * Function translite eng to rus
     */
    function translit($str) {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
    }
}
