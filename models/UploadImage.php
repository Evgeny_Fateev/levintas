<?php
/**
 * Created by PhpStorm.
 * User: zhenja
 * Date: 15.01.18
 * Time: 13:32
 */

namespace app\models;
use Yii;
use yii\base\Model;
use yii\imagine\Image;
use Imagine\Image\Box;
/**
 * UploadImage model
 * @package app\models
 */
class UploadImage extends Model
{
    public $image;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,jpeg,svg',
                'maxSize' => 1024 * 1024 * 7,
                'tooBig' => 'File is too big'],
        ];
    }

    /**
     * Upload image
     * @param string $type
     * @return null|string
     */
    public function upload(string $type)
    {
        if(!$this->validate()){
            return null;
        }
        // Select dir
        switch ($type){
            case 'news':
                $dir = '/img/news/';
                break;

            case 'main-product':
                $dir = '/img/main-product/';
                break;
            case 'all-product':
                $dir = '/img/all-product/';
                break;
            case 'image-category':
                $dir = '/img/image-category/';
                break;
            case 'image-sub_category':
                $dir = '/img/image-sub_category/';
                break;
            case 'slider':
                $dir = '/img/slider/';
                break;


            default:
                return null;
        }

        $rand_name = md5(microtime() . rand(0, 9999));
        $file = $dir . $rand_name . '.'. $this->image->extension;
        $this->image->saveAs(Yii::getAlias('@app/web'. $file));


        return $file;

    }

}