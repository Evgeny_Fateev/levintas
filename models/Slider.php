<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property int $id
 * @property string $image
 * @property string $title
 */
class Slider extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'title'], 'required', 'on' => self::SCENARIO_CREATE],
            [['image'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'title' => 'Заголовок слайда',
        ];
    }
}
