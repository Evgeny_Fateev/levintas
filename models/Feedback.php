<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $text
 * @property integer $is_confirm
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $is_confirm;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'text'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 1000],
            ['is_confirm','compare', 'compareValue' => 1, 'message' => 'Вы не согласились на обработку персональных данных']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'text' => 'Текст',
            'is_confirm' => 'Согласие на обработку персональных данных'
        ];
    }
}
