<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%application}}".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $text
 * @property string $ids_product
 * @property string $email
 * @property integer $is_confirm
 * @property integer $is_read
 */
class Application extends \yii\db\ActiveRecord
{
    public $is_confirm;

    const READ = 1;
    const NO_READ = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%application}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'text', 'ids_product'], 'required'],
            ['email', 'email'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            ['is_read', 'default', 'value' => self::NO_READ],
            [['text', 'ids_product'], 'string', 'max' => 1000],
            ['is_confirm','compare', 'compareValue' => 1, 'message' => 'Вы не согласились на обработку персональных данных']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ids_product' => 'Продукты',
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'text' => 'Комментарий',
            'is_confirm' => 'Согласие на обработку персональных данных',
            'is_read' => 'Статус',
            'email' => 'E-mail'
        ];
    }
}
