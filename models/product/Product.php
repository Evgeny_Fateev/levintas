<?php

namespace app\models\product;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property int $product_category_id
 * @property int $product_sub_category
 * @property int $product_type_id
 * @property string $image
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $new_price
 * @property string $preparation_time
 * @property string $gold_content
 * @property string $color
 * @property string $vendor_code
 * @property string $number_of_stones
 * @property string $product_weight
 * @property string $weight_of_stones
 * @property string $type_of_stones
 * @property string $sale
 * @property string $slug
 *
 * @property ProductImage[] $productImages
 */
class Product extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_category_id', 'product_sub_category', 'name', 'description', 'price', 'image', 'vendor_code'], 'required', 'on' => self::SCENARIO_CREATE],
            [['product_category_id', 'product_sub_category', 'product_type_id'], 'integer'],
            [['product_sub_category'], 'integer', 'min' => 1, 'tooSmall' => 'Не найдено категории'],
            [['price', 'new_price'], 'number'],
            ['sale', 'default', 'value' => null],
            [['name', 'slug'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 2000, 'on' => self::SCENARIO_UPDATE],
            [['preparation_time', 'vendor_code', 'color', 'gold_content', 'number_of_stones', 'product_weight', 'weight_of_stones', 'type_of_stones', 'sale'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_category_id' => 'Категория продукта',
            'product_sub_category' => 'Подкатегория продукта',
            'product_type_id' => 'Тип продукта',
            'image' => 'Главное изображение',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
            'new_price' => 'Цена со скидкой',
            'preparation_time' => 'Время изготовления',
            'gold_content' => 'Проба золота',
            'number_of_stones' => 'Количество камней',
            'product_weight' => 'Вес изделия',
            'weight_of_stones' => 'Вес камней',
            'type_of_stones' => 'Тип камней',
            'sale' => 'Скидка',
            'slug' => 'Slug',
            'vendor_code' => 'Артикул',
            'color' => 'Цвет изделия'
        ];
    }

    /**
     * Bevaiors create slug
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'immutable' => false,
                'ensureUnique'=> true,
                'value' => function($event){
                    return \yii\helpers\Inflector::slug($this->translit($event->sender->name));
                },
            ],

            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'vendor_code',
                'immutable' => false,
                'ensureUnique'=> true,
                'value' => function($event){
                    $product_max_id = Product::find()->max('id');
                    $product_max_id = $product_max_id + 1;
                    $first_cat_word = ProductCategory::findOne(['id' => $event->sender->product_category_id]);
                    $first_cat_word = $first_cat_word->name;
                    if(!empty($event->sender->id)){
                        return $this->translit(mb_substr($first_cat_word, 0, 1, "UTF-8")) .str_pad( $event->sender->id, 7, "0", STR_PAD_LEFT);
                    }
                    else {
                        return $this->translit(mb_substr($first_cat_word, 0, 1, "UTF-8")) . str_pad($product_max_id, 7, "0", STR_PAD_LEFT);
                    }
                },
            ],
        ];
    }

    /**
     * Function translite eng to rus
     */
    function translit($str) {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSubCategories()
    {
        return $this->hasOne(ProductSubCategory::className(), ['id' => 'product_sub_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTypes()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'product_type_id']);
    }
}
