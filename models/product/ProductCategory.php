<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_category}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $description_category
 * @property string $image
 * @property string $image_category
 *
 * @property ProductSubCategory[] $productSubCategories
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'image', 'image_category'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255,'on' => self::SCENARIO_UPDATE],
            [['description', 'description_category'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание категории',
            'description_category' => 'Описание на странице категории',
            'image' => 'Изображение на главной',
            'image_category' => 'Изображение на странице категории в шапке'

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSubCategories()
    {
        return $this->hasMany(ProductSubCategory::className(), ['product_category_id' => 'id']);
    }
}
