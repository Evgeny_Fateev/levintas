<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_sub_category}}".
 *
 * @property int $id
 * @property int $product_category_id
 * @property string $name
 * @property string $image
 *
 * @property ProductCategory $productCategory
 * @property ProductType[] $productTypes
 */
class ProductSubCategory extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = '1';
    const SCENARIO_UPDATE = '2';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_sub_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_category_id', 'name', 'image'], 'required', 'on' => self::SCENARIO_CREATE],
            [['product_category_id'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255, 'on' => self::SCENARIO_UPDATE],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_category_id' => 'Категория продукта',
            'name' => 'Название',
            'image' => 'Фоновое изображение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTypes()
    {
        return $this->hasMany(ProductType::className(), ['product_sub_category_id' => 'id']);
    }
}
