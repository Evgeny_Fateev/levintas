<?php

namespace app\models\product;

use Yii;

/**
 * This is the model class for table "{{%product_type}}".
 *
 * @property int $id
 * @property int $product_sub_category_id
 * @property string $name
 *
 * @property ProductSubCategory $productSubCategory
 */
class ProductType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_type}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_sub_category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['product_sub_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSubCategory::className(), 'targetAttribute' => ['product_sub_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_sub_category_id' => 'Подкатегория продукта',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSubCategory()
    {
        return $this->hasOne(ProductSubCategory::className(), ['id' => 'product_sub_category_id']);
    }
}
