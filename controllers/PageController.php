<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 20.06.18
 * Time: 12:10
 */

namespace app\controllers;


use app\models\Application;
use app\models\FormSubmit;
use app\models\Mailer;
use app\models\News;
use app\models\product\Product;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class PageController extends Controller
{
    public $layout = 'page-layout';

    public function actionServices(){
        $this->view->title = 'Услуги | LEVINTAS JEWERLY';
        $this->view->params['image_background'] = 'img/services-header-bg.jpg';
        return $this->render('services');
    }

    public function actionAbout(){
        $this->view->title = 'О нас | LEVINTAS JEWERLY';
        $this->view->params['image_background'] = 'img/about-header-bg.jpg';
        return $this->render('about');
    }

    public function actionContacts(){
        $this->view->title = 'Контакты | LEVINTAS JEWERLY';
        $this->view->params['image_background'] = 'img/contacts-header-bg.jpg';
        return $this->render('contacts');
    }

    public function actionNews(){
        $this->layout = 'news-layout';
        $this->view->title = 'Новости | LEVINTAS JEWERLY';
        $this->view->params['image_background'] = 'img/articles-img.jpg';
        $this->view->params['title'] = 'Новости';
        $this->view->params['subtitle'] = 'Кроме продажы готовых изделий наша компания занимается изготовлением уникальных украшений на заказ';
        $query_news = News::find()->where(['is_published' => News::PUBLISH]);
        $pages_news = new Pagination(['totalCount' => $query_news->count(), 'pageSize' => 6, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $news = $query_news->offset($pages_news->offset)->limit($pages_news->limit)->all();
        return $this->render('news', [
            'pages_news' => $pages_news,
            'news' => $news
        ]);
    }

    public function actionSingleNews($id){
        $this->layout = 'news-layout';
        $news = News::findOne($id);
        $this->view->title = $news->name;
        $this->view->params['image_background'] = $news->image;
        $this->view->params['title'] = $news->name;
        return $this->render('single-news', [
            'news' => $news
        ]);
    }

    public function actionBasket(){
        $this->layout = 'basket-layout';
        $this->view->title = 'Корзина | LEVINTAS JEWERLY';
        $model = new Application();
        if($model->load(Yii::$app->request->post())){
            $recaptcha_secret = "6Lev9WAUAAAAAJslvgj0YPRT__wvj4S8leHL_cYu";

            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

            $response = json_decode($response, true);

            if($response["success"] === true) {
                if($model->save()){
                    $name_order = '';
                    foreach (unserialize($model->ids_product) as $value){
                        $order = Product::findOne($value);
                        $name_order .= $order->name . ', ';
                    }

                    $mailer = new Mailer();
                    $mailer->sendApplication('Заявка',
                        $model->name,
                        $model->phone,
                        $name_order);
                    Yii::$app->response->cookies->remove('basket');
                    Yii::$app->session->set('applicationFormSubmitted', 'yes');
                    return $this->redirect('basket');
                }
            }

        }
        return $this->render('basket', [
            'model' => $model
        ]);
    }

}