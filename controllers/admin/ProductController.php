<?php

namespace app\controllers\admin;

use app\models\product\ProductCategory;
use app\models\product\ProductImage;
use app\models\product\ProductSubCategory;
use app\models\product\ProductType;
use app\models\UploadImage;
use app\models\User;
use Yii;
use app\models\product\Product;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLists($id)
    {
        $posts = ProductSubCategory::find()
            ->where(['product_category_id' => $id])
            ->all();

        if (!empty($posts)) {
            foreach($posts as $post) {
                echo "<option value='".$post->id."'>".$post->name."</option>";
            }
        } else {
            echo "<option value='0'>Для подкатегории не найдена категория</option>";
        }

    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $category = ProductCategory::find()->all();
        $sub_category = ProductSubCategory::find()->all();
        $type_category = ProductType::find()->all();
        $model->scenario = Product::SCENARIO_CREATE;
        $all_images = new ProductImage();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image;
                $model->image = $upload_form->upload('main-product');
            }


            if ($model->save()) {
                if($all_images->load(Yii::$app->request->post())){
                    $image_all_product = UploadedFile::getInstances($all_images, 'image');
                    $id = $model->id;
                    foreach ($image_all_product as $file){
                        $all_imagesc = new ProductImage();
                        $upload_all = new UploadImage();
                        $upload_all->image = $file;
                        $all_imagesc->image = $upload_all->upload('all-product');
                        $all_imagesc->product_id = $id;
                        if(!$all_imagesc->save()){
                            var_dump($all_imagesc->getErrors());die();
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'category' => $category,
            'sub_category' => $sub_category,
            'type_category' => $type_category,
            'all_images' => $all_images
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Product::SCENARIO_UPDATE;
        $category = ProductCategory::find()->all();
        $sub_category = ProductSubCategory::find()->all();
        $type_category = ProductType::find()->all();
        $all_images = new ProductImage();
        $view_all_images = ProductImage::findAll(['product_id' => $id]);
        $tmp_image_name = $model->image;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image;
                $image_name = $upload_form->upload('main-product');

                if($tmp_image_name != null || ''){
                    unlink(Yii::getAlias('@app/web'. $tmp_image_name));
                }

            }
            $model->image = $image_name ?? $tmp_image_name;

            if ($model->save()) {
                if($all_images->load(Yii::$app->request->post())){
                    $image_all_product = UploadedFile::getInstances($all_images, 'image');
                    $id = $model->id;
                    foreach ($image_all_product as $file){
                        $all_imagesc = new ProductImage();
                        $upload_all = new UploadImage();
                        $upload_all->image = $file;
                        $all_imagesc->image = $upload_all->upload('all-product');
                        $all_imagesc->product_id = $id;
                        if(!$all_imagesc->save()){
                            var_dump($all_imagesc->getErrors());die();
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'category' => $category,
            'sub_category' => $sub_category,
            'type_category' => $type_category,
            'all_images' => $all_images,
            'view_all_images' => $view_all_images
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = Product::findOne($id);
        $all_image = ProductImage::findAll(['product_id' => $id]);
        foreach ($all_image as $value){
            if($value->image !== null){
                unlink(Yii::getAlias('@app/web'. $value->image));
            }
        }

        $this->findModel($id)->delete();
        if($model->image !== null){
            unlink(Yii::getAlias('@app/web'. $model->image));
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteImage(){
        if(Yii::$app->request->post()){
            $model = ProductImage::findOne(Yii::$app->request->post('id'));
            $model->delete();
            if($model->image !== null){
                unlink(Yii::getAlias('@app/web'. $model->image));
            }
            return true;
        }
        return false;
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
