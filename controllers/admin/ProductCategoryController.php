<?php

namespace app\controllers\admin;

use app\models\UploadImage;
use app\models\User;
use Yii;
use app\models\product\ProductCategory;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductCategory::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductCategory();
        $model->scenario = ProductCategory::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image_cat = UploadedFile::getInstance($model, 'image_category');
            if ($image !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image;
                $model->image = $upload_form->upload('image-category');
            }

            if ($image_cat !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image_cat;
                $model->image_category = $upload_form->upload('image-category');
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = ProductCategory::SCENARIO_UPDATE;
        $tmp_image_name = $model->image;
        $tmp_image_name_category = $model->image_category;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image_cat = UploadedFile::getInstance($model, 'image_category');
            if ($image !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image;
                $image_name = $upload_form->upload('image-category');

                if($tmp_image_name != null || ''){
                    unlink(Yii::getAlias('@app/web'. $tmp_image_name));
                }

            }

            if ($image_cat !== null) {
                $upload_form = new UploadImage();
                $upload_form->image = $image_cat;
                $image_name_cat = $upload_form->upload('image-category');

                if($tmp_image_name_category != null || ''){
                    unlink(Yii::getAlias('@app/web'. $tmp_image_name_category));
                }

            }

            $model->image = $image_name ?? $tmp_image_name;
            $model->image_category = $image_name_cat ?? $tmp_image_name_category;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = ProductCategory::findOne($id);
        $this->findModel($id)->delete();
        if($model->image !== null){
            unlink(Yii::getAlias('@app/web'. $model->image));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
