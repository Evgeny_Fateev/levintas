<?php

namespace app\controllers\admin;
use app\models\Application;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;


class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(){
        return $this->render('/admin/index', [
            'count_application' => Application::find()->where(['is_read' => Application::NO_READ])->count()
        ]);
    }

}