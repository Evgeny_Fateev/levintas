<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 20.06.18
 * Time: 1:22
 */

namespace app\controllers;


use app\models\product\Product;
use app\models\product\ProductCategory;
use app\models\product\ProductImage;
use app\models\product\ProductSubCategory;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\Cookie;

class ProductController extends Controller
{
    public $layout = 'product-layout';
    public function actionIndex($id){

        $product_sub_category = ProductSubCategory::findAll(['product_category_id' => $id]);
        $products = Product::find()->where(['product_category_id' => $id])
        ->andWhere(['product_sub_category' => $product_sub_category[0]['id']])
            ->limit(8)->all();
//        $pages_products = new Pagination(['totalCount' => $query_products->count(), 'pageSize' => 16, 'forcePageParam' => false, 'pageSizeParam' => false]);
//        $products = $query_products->offset($pages_products->offset)->limit($pages_products->limit)->all();
        $category = ProductCategory::findOne($id);
        $this->view->title = $category->name . ' | LEVINTAS JEWERLY';
        $this->view->params['category_image'] = $category->image_category ?? $category->image;
        $this->view->params['category'] = $category->name;
        $this->view->params['description'] = $category->description_category;
        $count_product_category = count($products);
        $declension_view =  $this->declension_word($count_product_category);
        return $this->render('index', [
            'products' => $products,
            'count_product_category' => $count_product_category,
            'product_sub_category' => $product_sub_category,
            'declension_view' => $declension_view,
            'category' => $category
        ]);
    }

    public function actionLoadProduct(){
        if(Yii::$app->request->get()){
            $product_cat_id = Yii::$app->request->get('category_id');
            $product_sub_cat_id = Yii::$app->request->get('sub_category_id');
            $num = Yii::$app->request->get('num');
            $products = Product::find()->where(['product_category_id' => $product_cat_id])
                ->andWhere(['product_sub_category' => $product_sub_cat_id])
                ->offset($num)
                ->limit($num)->all();
            return $this->renderPartial('load-product', [
                'products' => $products
            ]);
        }
    }

    public function actionProduct($id){
        $product = Product::findOne($id);
        $this->view->title = $product->name . ' | LEVINTAS JEWERLY';
        $this->view->params['product_id'] = $product->id;
        $this->view->params['product_image'] = $product->image;
        $this->view->params['product_image_fon'] = $product->productCategory->image;
        $this->view->params['product_name'] = $product->name;
        $this->view->params['product_images'] = ProductImage::findAll(['product_id' => $id]);
        $this->view->params['product_price'] = $product->price;
        $this->view->params['product_weight'] = $product->product_weight;
        $this->view->params['number_of_stones'] = $product->number_of_stones;
        $this->view->params['weight_of_stones'] = $product->weight_of_stones;
        $this->view->params['preparation_time'] = $product->preparation_time;
        $this->view->params['gold_content'] = $product->gold_content;
        $this->view->params['vendor_code'] = $product->vendor_code;
        if(!is_null($product->sale) && !is_null($product->new_price)){
            $this->view->params['product_sale'] = $product->sale;
            $this->view->params['product_new_price'] = $product->new_price;
        }

        return $this->render('product', [
            'product' => $product
        ]);
    }

    public function actionLoadSubcategory(){
        if(Yii::$app->request->post()){
//            if(Yii::$app->request->post('id') != 0){
//                $query_products = Product::find()->where(['product_sub_category' => Yii::$app->request->post('id'),
//                    'product_category_id' => Yii::$app->request->post('cat_id')]);
//                $pages_products = new Pagination(['route' => '/product/index?id='.Yii::$app->request->post('cat_id'), 'totalCount' => $query_products->count(), 'pageSize' => 16, 'forcePageParam' => false, 'pageSizeParam' => false]);
//                $products = $query_products->offset($pages_products->offset)->limit($pages_products->limit)->all();
//
//            }
//            else{
//                $query_products = Product::find()->where(['product_category_id' => Yii::$app->request->post('cat_id')]);
//                $pages_products = new Pagination(['route' => '/product/index?id='.Yii::$app->request->post('cat_id'), 'totalCount' => $query_products->count(), 'pageSize' => 16, 'forcePageParam' => false, 'pageSizeParam' => false]);
//                $products = $query_products->offset($pages_products->offset)->limit($pages_products->limit)->all();
//
//            }

            $products = Product::find()->where(['product_category_id' => Yii::$app->request->post('cat_id')])
                ->andWhere(['product_sub_category' => Yii::$app->request->post('id')])
                ->limit(8)->all();

            return $this->renderPartial('load-subcategory', [
                'products' => $products,
                'sub_category_id' => Yii::$app->request->post('id'),
                'category_id' => Yii::$app->request->post('cat_id')
            ]);
        }
        return false;
    }

    public function actionLoadPrice(){
        if(Yii::$app->request->post()){
            if(Yii::$app->request->post('sub_cat_id') != 0){
                $query_products_not_sale = Product::find()
                    ->where(['product_category_id' => Yii::$app->request->post('cat_id')])
                    ->andWhere(['sale' => null])
                    ->andWhere(['>=', 'price', Yii::$app->request->post('start')])
                    ->andWhere(['<=', 'price', Yii::$app->request->post('end')])
                    ->andWhere(['product_sub_category' => Yii::$app->request->post('sub_cat_id')]);
                $query_products = Product::find()
                    ->where(['product_category_id' => Yii::$app->request->post('cat_id')])
                    ->andWhere(['not', ['sale' => null]])
                    ->andWhere(['>=', 'new_price', Yii::$app->request->post('start')])
                    ->andWhere(['<=', 'new_price', Yii::$app->request->post('end')])
                    ->andWhere(['product_sub_category' => Yii::$app->request->post('sub_cat_id')])
                    ->union($query_products_not_sale);
            }
            else{
                $query_products_not_sale = Product::find()
                    ->where(['product_category_id' => Yii::$app->request->post('cat_id')])
                    ->andWhere(['sale' => null])
                    ->andWhere(['>=', 'price', Yii::$app->request->post('start')])
                    ->andWhere(['<=', 'price', Yii::$app->request->post('end')]);
                $query_products = Product::find()
                    ->where(['product_category_id' => Yii::$app->request->post('cat_id')])
                    ->andWhere(['not', ['sale' => null]])
                    ->andWhere(['>=', 'new_price', Yii::$app->request->post('start')])
                    ->andWhere(['<=', 'new_price', Yii::$app->request->post('end')])
                    ->union($query_products_not_sale);
            }

            $pages_products = new Pagination(['totalCount' => $query_products->count(), 'pageSize' => 4, 'forcePageParam' => false, 'pageSizeParam' => false]);
            $products = $query_products->offset($pages_products->offset)->limit($pages_products->limit)->all();

            return $this->renderPartial('load-price', [
                'products' => $products,
                'pages_products' => $pages_products
            ]);
        }
        return false;
    }

    public function actionAddBasket(){
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id_product');
            $array_count = Yii::$app->request->cookies->getValue('basket');//сooc->get('basket');
            if(empty($array_count)){
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'basket',
                    'value' => array((int)$id),
                ]));
                return true;
            }
            else{
                if(!in_array($id,$array_count)){
                    $array_merge =  array_merge($array_count, array((int)$id));
                    Yii::$app->response->cookies->remove('basket');
                    Yii::$app->response->cookies->add(new Cookie([
                        'name' => 'basket',
                        'value' => $array_merge,
                    ]));
                    return true;
                }
            }
        }

        return false;
    }

    public function actionDeleteBasket(){
        if(Yii::$app->request->post()){
            $id = Yii::$app->request->post('id_product');
            $array_count = Yii::$app->request->cookies->getValue('basket');
            if(empty($array_count)){
                return false;
            }
            else{
                if(in_array($id, $array_count)){
                    $key = array_search($id, Yii::$app->request->cookies->getValue('basket'));
                    unset($array_count[$key]);
                    Yii::$app->response->cookies->remove('basket');
                    Yii::$app->response->cookies->add(new Cookie([
                        'name' => 'basket',
                        'value' => $array_count,
                    ]));
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Function declension word
     *
     * @param $number
     * @param array $word
     * @return mixed
     */
    function declension_word($number, $word = ['изделие', 'изделия', 'изделий']) {
        $ar= array (2, 0, 1, 1, 1, 2);
        return $word[ ($number%100 > 4 && $number%100 < 20) ? 2 : $ar[min($number%10, 5)] ];
    }
}