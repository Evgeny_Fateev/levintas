<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=levinth1_db',
    'username' => 'levinth1_db',
    'password' => 'levinth1_db',
    'charset' => 'utf8',

//     Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
