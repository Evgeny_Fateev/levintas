<?php

use yii\db\Migration;

/**
 * Handles adding image to table `product_sub_category`.
 */
class m180917_180038_add_image_column_to_product_sub_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_sub_category', 'image', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_sub_category', 'image');
    }
}
