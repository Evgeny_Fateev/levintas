<?php

use yii\db\Migration;

/**
 * Handles adding new_price to table `product`.
 */
class m180621_064151_add_new_price_column_to_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'new_price', $this->double()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'new_price');
    }
}
