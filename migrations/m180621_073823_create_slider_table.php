<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m180621_073823_create_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'image' => $this->string(255)->notNull(),
            'title' => $this->string(500)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slider');
    }
}
