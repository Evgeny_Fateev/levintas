<?php

use yii\db\Migration;

/**
 * Class m180628_210907_update_column_to_news_table
 */
class m180628_210907_update_column_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('news','full_description', $this->text()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('news','full_description', $this->string(2000)->notNull());
    }


}
