<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application`.
 */
class m180622_083148_create_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('application', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'phone' => $this->string(255)->notNull(),
            'text' => $this->string(1000)->notNull(),
            'ids_product' => $this->string(1000)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('application');
    }
}
