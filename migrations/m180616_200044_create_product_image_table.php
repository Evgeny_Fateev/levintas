<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_image`.
 */
class m180616_200044_create_product_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_image', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'image' => $this->string(255)->notNull(),
        ]);
        $this->createIndex(
            'idx-product-image',
            'product_image',
            'product_id'
        );
        $this->addForeignKey(
            'fk-product-image',
            'product_image',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_image');
    }
}
