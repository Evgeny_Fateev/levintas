<?php

use yii\db\Migration;

/**
 * Handles adding image_category to table `product_category`.
 */
class m180624_210759_add_image_category_column_to_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_category', 'image_category', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'image_category');

    }
}
