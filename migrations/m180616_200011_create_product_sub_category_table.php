<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_sub_category`.
 */
class m180616_200011_create_product_sub_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_sub_category', [
            'id' => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->createIndex(
            'idx-product-category_id',
            'product_sub_category',
            'product_category_id'
        );
        $this->addForeignKey(
            'fk-product-category_id',
            'product_sub_category',
            'product_category_id',
            'product_category',
            'id'
        );
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_sub_category');
    }
}
