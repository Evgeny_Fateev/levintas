<?php

use yii\db\Migration;

/**
 * Handles adding description_category to table `product_category`.
 */
class m180625_201137_add_description_category_column_to_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_category', 'description_category', $this->string(500));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'description_category');
    }
}
