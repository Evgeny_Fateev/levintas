<?php

use yii\db\Migration;

/**
 * Handles adding is_read to table `application`.
 */
class m180627_094332_add_is_read_column_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'is_read', $this->smallInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'is_read');
    }
}
