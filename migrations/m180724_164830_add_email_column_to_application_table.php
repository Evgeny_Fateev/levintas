<?php

use yii\db\Migration;

/**
 * Handles adding email to table `application`.
 */
class m180724_164830_add_email_column_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'email', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'email');
    }
}
