<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180616_200035_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'product_category_id' => $this->integer()->notNull(),
            'product_sub_category' => $this->integer()->notNull(),
            'product_type_id' => $this->integer(),
            'image' => $this->string(255)->notNull(),
            'name' => $this->string(500)->notNull(),
            'description' => $this->string(2000)->notNull(),
            'price' => $this->double()->notNull(),
            'preparation_time' => $this->string(255),
            'gold_content' => $this->string(255),
            'number_of_stones' => $this->string(255),
            'product_weight' => $this->string(255),
            'weight_of_stones' => $this->string(255),
            'type_of_stones' => $this->string(255),
            'sale' => $this->string(255),
            'slug' => $this->string(500)
        ]);
        $this->createIndex(
            'idx-product_cat_id',
            'product',
            'product_category_id'
        );
        $this->createIndex(
            'idx-product_sub_cat_id',
            'product',
            'product_sub_category'
        );
        $this->createIndex(
            'idx-product_type_id',
            'product',
            'product_type_id'
        );
        $this->createIndex(
            'idx-product_price',
            'product',
            'price'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
