<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_type`.
 */
class m180616_200024_create_product_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_type', [
            'id' => $this->primaryKey(),
            'product_sub_category_id' => $this->integer(),
            'name' => $this->string(255)
        ]);

        $this->createIndex(
            'idx-product-sub_category_id',
            'product_type',
            'product_sub_category_id'
        );
        $this->addForeignKey(
            'fk-product-sub_category_id',
            'product_type',
            'product_sub_category_id',
            'product_sub_category',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_type');
    }
}
