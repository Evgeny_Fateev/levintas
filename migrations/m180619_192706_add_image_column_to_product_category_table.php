<?php

use yii\db\Migration;

/**
 * Handles adding image to table `product_category`.
 */
class m180619_192706_add_image_column_to_product_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_category', 'image', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_category', 'image');
    }
}
