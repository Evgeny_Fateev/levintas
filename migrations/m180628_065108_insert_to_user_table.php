<?php

use yii\db\Migration;

/**
 * Class m180628_065108_insert_to_user_table
 */
class m180628_065108_insert_to_user_table extends Migration
{
    public function up()
    {
        $data_text = [
            [
                'username' => 'admin',
                'email' => 'Levintasjewerly@mail.ru',
                'password_hash' => '$2y$13$P5zxrG0NJTz8dozcaF19Muk.TT.3FpBDBzf5bwiz56Zm/g6OAfHcS',
                'auth_key' => Yii::$app->security->generateRandomString(),
                'is_admin' => 1,
                'created_at' => strtotime(Yii::$app->formatter->asDatetime('now', 'php: Y-m-d H:i:s')),
                'updated_at' => strtotime(Yii::$app->formatter->asDatetime('now', 'php: Y-m-d H:i:s'))
            ],

        ];
        foreach ($data_text as $text) {
            $this->insert('user', [
                'username' => $text['username'],
                'email' => $text['email'],
                'password_hash' => $text['password_hash'],
                'auth_key' => $text['auth_key'],
                'is_admin' => $text['is_admin'],
                'created_at' => $text['created_at'],
                'updated_at' => $text['updated_at'],
            ]);
        }
    }

    public function down()
    {
        $this->truncateTable('user');
    }
}
