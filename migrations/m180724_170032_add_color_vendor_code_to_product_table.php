<?php

use yii\db\Migration;

/**
 * Class m180724_170032_add_color_vendor_code_to_product_table
 */
class m180724_170032_add_color_vendor_code_to_product_table extends Migration
{
    /**
 * {@inheritdoc}
 */
    public function safeUp()
    {
        $this->addColumn('product', 'color', $this->string(255));
        $this->addColumn('product', 'vendor_code', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product', 'color');
        $this->dropColumn('product', 'vendor_code');
    }

}
