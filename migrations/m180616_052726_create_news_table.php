<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180616_052726_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'image' => $this->string(255)->notNull(),
            'short_description' => $this->string(500)->notNull(),
            'full_description' => $this->string(2000)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'is_published' => $this->smallInteger()->defaultValue(0),
            'slug' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
