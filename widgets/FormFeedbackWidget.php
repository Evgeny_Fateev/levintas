<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 21.06.18
 * Time: 14:32
 */

namespace app\widgets;


use app\models\Feedback;
use app\models\Mailer;
use yii\base\Widget;
use Yii;

class FormFeedbackWidget extends Widget
{
    public function run()
    {
        $feedback = new Feedback();
        if($feedback->load(Yii::$app->request->post())){
            $recaptcha_secret = "6Lev9WAUAAAAAJslvgj0YPRT__wvj4S8leHL_cYu";

            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

            $response = json_decode($response, true);

            if($response["success"] === true) {
                if ($feedback->save()){
                    $mailer = new Mailer();
                    $mailer->sendFeedback('Обратная связь',
                        $feedback->name,
                        $feedback->phone,
                        $feedback->text);
                    Yii::$app->session->set('applicationFormSubmitted', 'yes');
                    Yii::$app->getResponse()->redirect('/contacts')->send();
                    Yii::$app->end();
                }
            }

        }
        return $this->render('form-feedback', [
            'feedback' => $feedback
        ]);
    }

}