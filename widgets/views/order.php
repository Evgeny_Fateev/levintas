<?php
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>

<div class="card__form js-card-form">
    <button class="js-card-btn-back card__arrow-wrap">
        <span class="card__arrow"></span>
    </button>

    <?php

    $form = ActiveForm::begin([
        'id' =>'form_application',
        'options' => [
            'class' => 'contacts-page__form form'
        ],
        'validateOnBlur' => false
    ]);?>
    <?= $form->field($model, 'ids_product')
        ->hiddenInput() ?>

    <div class="contacts-page__form-left">
        <?= $form->field($model, 'text')->textarea(['placeholder' => 'Введите комментарий']) ?>
    </div>
    <div class="contacts-page__form-right">
        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Введите имя']) ?>
        <?= $form->field($model, 'email')->textInput(['placeholder' => 'Введите email']) ?>
        <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
            'mask' => '+7 (999) 999-9999',
            'options' => [
                'class' => 'form-control',
                'placeholder' => ('Введите номер телефона')
            ],
            'clientOptions' => [
                'clearIncomplete' => true
            ]

        ])?>
        <?= $form->field($model, 'is_confirm', ['options' => ['class' => 'form__check']])
            ->checkbox(['label' => '<span>Я соглашаюсь на обработку персональных данных</span>']); ?>

        <!--    <input type="text">-->
        <!--    <input type="email">-->
        <!--    <label class="form__check">-->
        <!--        <input type="checkbox" name="agree">-->
        <!--        <span>Согласие на обработку персональных данных</span>-->
        <!--    </label>-->
        <input class="g-recaptcha"
               data-sitekey="6Lev9WAUAAAAAPqfp7Pdc8R54DN8V9Uj8jC49X36"
               data-callback="onSubmitApplication" type="submit" value="Отправить">
    </div>
    <?php ActiveForm::end();?>
</div>
