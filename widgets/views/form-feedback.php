<?php
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>я

<?php $form = ActiveForm::begin([
    'id' =>'form_feedback',
    'options' => [
        'class' => 'contacts-page__form form'
    ],
    'validateOnBlur' => false
]);?>
<div class="contacts-page__form-left">
    <?= $form->field($feedback, 'text')->textarea(['placeholder' => 'Введите текст']) ?>
</div>

<div class="contacts-page__form-right">
    <?= $form->field($feedback, 'name')->textInput(['placeholder' => 'Введите имя']) ?>
    <?= $form->field($feedback, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+7 (999) 999-9999',
        'options' => [
            'class' => 'form-control',
            'placeholder' => ('Введите номер телефона')
        ],
        'clientOptions' => [
            'clearIncomplete' => true
        ]

    ])?>
    <?= $form->field($feedback, 'is_confirm', ['options' => ['class' => 'form__check']])
        ->checkbox(['label' => '<span>Я соглашаюсь на обработку персональных данных</span>']); ?>
<!--    <input type="text">-->
<!--    <input type="email">-->
<!--    <label class="form__check">-->
<!--        <input type="checkbox" name="agree">-->
<!--        <span>Согласие на обработку персональных данных</span>-->
<!--    </label>-->
    <input class="g-recaptcha"
           data-sitekey="6Lev9WAUAAAAAPqfp7Pdc8R54DN8V9Uj8jC49X36"
           data-callback="onSubmitFeedback" type="submit" value="Отправить">
</div>
<?php ActiveForm::end();?>
