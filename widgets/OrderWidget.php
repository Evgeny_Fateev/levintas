<?php
/**
 * Created by PhpStorm.
 * User: evgeny
 * Date: 24.07.18
 * Time: 22:38
 */

namespace app\widgets;


use app\models\Application;
use app\models\Mailer;
use app\models\product\Product;
use Yii;
use yii\base\Widget;

class OrderWidget extends Widget
{
    public function run()
    {
        $model = new Application();
        if($model->load(Yii::$app->request->post())){
            $recaptcha_secret = "6Lev9WAUAAAAAJslvgj0YPRT__wvj4S8leHL_cYu";

            $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

            $response = json_decode($response, true);

            if($response["success"] === true) {
                $name_order = '';
                $order = Product::findOne($model->ids_product);
                $name_order .= $order->name . ' Артикул: ' . $order->vendor_code;

                if ($model->save()){
                    $mailer = new Mailer();
                    $mailer->sendApplication('Заявка',
                        $model->name,
                        $model->phone,
                        $model->email,
                        $name_order);
                    Yii::$app->response->cookies->remove('basket');
                    Yii::$app->session->set('applicationFormSubmitted', 'yes');
                    Yii::$app->getResponse()->redirect('/')->send();
                    Yii::$app->end();
                }
            }

        }
        return $this->render('order', [
            'model' => $model
        ]);
    }

}